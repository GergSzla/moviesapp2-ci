# Agile Software Practice Assignment 2 
## Automate the CI/CD process for a web API project.
BSc (Hons) Software Systems Development,
Gergo Szilagyi,
20079738,
Year 4.

## Features
- Successfully Installs Modules
- Successfully Builds API

## Failures
- Fails Tests
- Fails Staging (Continuous Deployment)
### Install
![](https://res.cloudinary.com/dkdptqakb/image/upload/v1610881908/AGILE/inst_success.png)
>The Install job of the app installs everything successfully without any difficulty. 

### Build
![](https://res.cloudinary.com/dkdptqakb/image/upload/v1610881908/AGILE/build_api_success.png)
>The build job builds the TMDB API successfully as demonstrated in the screenshot above .

### Test
![](https://res.cloudinary.com/dkdptqakb/image/upload/v1610881908/AGILE/test_failed.png)
>The test job unfortunately fails as it fails to load user data and movies.

### Heroku
![](https://res.cloudinary.com/dkdptqakb/image/upload/v1610882049/AGILE/heroku.png)
>Unfortunatly, because the test job failed before, so did the pipeline. Meaning the staging via Heroku could not be completed successfully. The above screenshot shows the most recent updates on heroku.

## Branching Policy
### Development
Development branch was used for the creation of this project. This branch contains all jobs except the automatic deploy to production environment. 

## Links
- [GitLab](https://gitlab.com/GergSzla/moviesapp2-ci)
- [YouTube](https://www.youtube.com/watch?v=AOY34T-gxfg)
